FROM openjdk:11

ARG version

ENV version=$version

COPY target/assignment-${version}.jar  APP.jar

ENV JAVA_OPTS="-Xms1024m -Xmx2048m -Djava.awt.headless=true -Dfile.encoding=UTF-8 "

ENTRYPOINT java -jar ${JAVA_OPTS} -Dspring.datasource.username=${SPRING_DATASOURCE_USERNAME} -Dspring.datasource.password=${SPRING_DATASOURCE_PASSWORD} -Dspring.datasource.url=${SPRING_DATASOURCE_URL} APP.jar



