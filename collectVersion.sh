#!/bin/sh
##
sha=$(git rev-parse --short=8 HEAD -s)
date=$(git log -1 --format="%at" | xargs -I{} date -d @{} +%Y%m%d)
echo ${sha}-${date}